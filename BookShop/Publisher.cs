using System;
using System.IO;


public class Publisher
{
    private Random rand;


    public Publisher()
    {
        rand = new Random();
    }


    public Book[] AskForBooks()
    {
        return ProduceBookBundle();
    }


    private Book[] ProduceBookBundle()
    {
        int nbook = rand.Next(10, 50);
        Book[] arr = new Book[nbook];

        for (int i = 0; i < nbook; i++)
            arr[i] = ProduceBook();

        return arr;
    }


    private Book ProduceBook()
    {
        string[] allFileNames = Directory.GetFiles("book-templates");
        if (allFileNames.Length == 0)
        {
            throw new InvalidProgramException("no templete files found");
        }

        int randIndex = rand.Next(allFileNames.Length);
        string fileName = allFileNames[randIndex];

        string[] lines = File.ReadAllLines(fileName);

        int pageCount = int.Parse(lines[2]);
        int year = int.Parse(lines[3]);
        bool hardCover = bool.Parse(lines[4]);
        float price = float.Parse(lines[5]);

        return new Book(lines[0], lines[1], year, pageCount, hardCover, price);
    }
}