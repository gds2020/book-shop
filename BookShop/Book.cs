﻿using System;

public class Book
{
    private string name;
    private string author;
    private int pageCount;
    private int year;
    private bool isHardcover;
    private float price;




    public Book(string Name, string Author, int Year, int PageCount, bool IsHardcover, float Price)
    {
        if (Name == null || Name.Length == 0)
            PrintErrorAndExit("invalid book name");

        name = Name;

        if (string.IsNullOrEmpty(Author))
            PrintErrorAndExit("invalid author name");

        author = Author;

        if (Year < 800)
            PrintErrorAndExit("book year is invalid");

        year = Year;

        if (PageCount < 1)
            PrintErrorAndExit("page count can't be < 1");

        pageCount = PageCount;

        isHardcover = IsHardcover;

        if (Price < 1f)
            PrintErrorAndExit("parice can't be < 1.0");

        price = Price;
    }

    public void PrintInfo()
    {
        string str = string.Format("\"{0}\" by {1}, {2}", name, author, year);
        Console.WriteLine(str);
        str = string.Format("hardcover: {1}, {0} pages", pageCount, isHardcover);
        Console.WriteLine(str);
        Console.WriteLine(string.Format("price: ${0:0.00}", price));
    }

    public void PrintShortInfo()
    {
        Console.WriteLine(string.Format("{0} ${1:0.00}", name, price));
    }

    public float GetPrice()
    {
        return price;
    }

    public string GetName()
    {
        return name;
    }

    private void PrintErrorAndExit(string errorMessage)
    {
        Console.WriteLine(errorMessage);
        Console.WriteLine("book name: " + name);
        Environment.Exit(1);
    }

}
