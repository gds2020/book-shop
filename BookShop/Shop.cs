﻿using System;
using System.Collections.Generic;

public class Shop
{
    const float PUBLISHER_DISCOUNT = 0.7f;
    const int MIN_BOOKS = 3;

    private Book[] depot;
    private int count;
    private float ballance;
    private Publisher publisher;


    public Shop(Publisher pub)
    {
        depot = new Book[8];
        count = 0;
        ballance = 1000f;
        if (pub == null)
            throw new ArgumentNullException("publisher can't be null");
        publisher = pub;
        PurchaseBooks();
    }


    public void AddBook(Book book)
    {
        bool isEnoughSpace = count < depot.Length;
        if (!isEnoughSpace)
            EnlargeDepotCapacity();

        depot[count] = book;
        count++;
        ballance -= book.GetPrice() * PUBLISHER_DISCOUNT;
    }


    public void AddBook(Book[] books)
    {
        for (int i = 0; i < books.Length; i++)
        {
            AddBook(books[i]);
        }
    }


    public Book BuyBook(int index)
    {
        if ((index >= 0 && index < count) == false)
            return null;

        Book book = depot[index];
        for (int i = index; i < count-1; i++)
            depot[i] = depot[i + 1];

        depot[count - 1] = null;
        count--;
        ballance += book.GetPrice();

        if (count < MIN_BOOKS)
        {
            PurchaseBooks();
        }

        return book;
    }


    public Book BuyBook(string name)
    {
        for (int i = 0; i < count; i++)
        {
            if (name == depot[i].GetName())
            {
                return BuyBook(i);
            }
        }
        return null;
    }


    public void PrintStock(bool isLongInfo)
    {
        string separator = "".PadLeft(50, '.');
        for (int i = 0; i < count; i++)
        {
            if (isLongInfo)
            {
                depot[i].PrintInfo();
                Console.WriteLine(separator);
            }
            else
            {
                depot[i].PrintShortInfo();
            }
        }
    }


    private void EnlargeDepotCapacity()
    {
        Book[] newDepot = new Book[depot.Length * 2];

        for (int i = 0; i < depot.Length; i++)
            newDepot[i] = depot[i];

        depot = newDepot;
    }


    private void PurchaseBooks()
    {
        Book[] bundle = publisher.AskForBooks();
        AddBook(bundle);
    }
}
