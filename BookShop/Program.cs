﻿using System;


public class Program
{
    static void Main(string[] args)
    {
        Publisher publisher = new Publisher();
        Shop shop = new Shop(publisher);

        shop.BuyBook(0).PrintInfo();
    }
}

